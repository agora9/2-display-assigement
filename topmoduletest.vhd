--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:52:16 09/24/2013
-- Design Name:   
-- Module Name:   C:/alma/temp/topmoduletest.vhd
-- Project Name:  twodisplayassig
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: topmodule
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY topmoduletest IS
END topmoduletest;
 
ARCHITECTURE behavior OF topmoduletest IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT topmodule
    PORT(
         sixhundred : IN  std_logic;
         reset : IN  std_logic;
         seg_final : OUT  std_logic_vector(7 downto 0);
         om : INOUT  std_logic_vector(3 downto 0);
         switch : INOUT  std_logic_vector(1 downto 0);
         seg : INOUT  std_logic_vector(7 downto 0);
         a : INOUT  std_logic_vector(3 downto 0);
         b : INOUT  std_logic_vector(3 downto 0);
         c : INOUT  std_logic_vector(3 downto 0);
         d : INOUT  std_logic_vector(3 downto 0);
         strobe : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal sixhundred : std_logic := '0';
   signal reset : std_logic := '0';

	--BiDirs
   signal om : std_logic_vector(3 downto 0);
   signal switch : std_logic_vector(1 downto 0);
   signal seg : std_logic_vector(7 downto 0);
   signal a : std_logic_vector(3 downto 0);
   signal b : std_logic_vector(3 downto 0);
   signal c : std_logic_vector(3 downto 0);
   signal d : std_logic_vector(3 downto 0);

 	--Outputs
   signal seg_final : std_logic_vector(7 downto 0);
   signal strobe : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: topmodule PORT MAP (
          sixhundred => sixhundred,
          reset => reset,
          seg_final => seg_final,
          om => om,
          switch => switch,
          seg => seg,
          a => a,
          b => b,
          c => c,
          d => d,
          strobe => strobe
        );

  
 

   -- Stimulus process
   stim_proc: process
   begin		

for I in 0 to 900000 loop
sixhundred <= '1';
wait for 1 ns;
sixhundred <= '0';
wait for 1 ns;
end loop;	
      -- hold reset state for 100 ns.
      wait for 100 ns;	

     

      -- insert stimulus here 

      wait;
   end process;

END;
