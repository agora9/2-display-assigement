----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:25:22 09/23/2013 
-- Design Name: 
-- Module Name:    bcd27segmdecod - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcd27segmdecod is
    Port ( om : in  STD_LOGIC_VECTOR (3 downto 0);
           seg : out  STD_LOGIC_VECTOR (7 downto 0));
end bcd27segmdecod;

architecture Behavioral of bcd27segmdecod is

begin

with om select

seg <= "11111100" when "0000", -- 0
	"01100000" when "0001", -- 1
	"11011010" when "0010", -- 2
	"11110010" when "0011", -- 3
	"01100110" when "0100", -- 4
	"10110110" when "0101", -- 5
	"10111110" when "0110", -- 6
	"11100000" when "0111", -- 7
	"11111110" when "1000", -- 8
	"11110110" when "1001", -- 9
	"00000001" when others; -- invalid combination


end Behavioral;

