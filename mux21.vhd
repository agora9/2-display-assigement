----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:37:21 09/23/2013 
-- Design Name: 
-- Module Name:    mux21 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux21 is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
			  c : in  STD_LOGIC_VECTOR (3 downto 0);
			  d : in  STD_LOGIC_VECTOR (3 downto 0);
			  strobe : out  STD_LOGIC_VECTOR (3 downto 0);
			  switch : in  STD_LOGIC_VECTOR (1 downto 0);
           om : inout  STD_LOGIC_VECTOR (3 downto 0));
end mux21;

architecture Behavioral of mux21 is

begin

Process (switch)
begin

if (switch = "00") then
om <= a;
strobe <= "0111";
End if;

if (switch = "01") then
om <= b;
strobe <= "1011";
End if;

if (switch = "10") then
om <= c;
strobe <= "1101";
End if;

if (switch = "11") then
om <= d;
strobe <= "1110";
End if;

end process;
end Behavioral;

