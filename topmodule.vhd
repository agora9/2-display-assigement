----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:14:32 09/24/2013 
-- Design Name: 
-- Module Name:    topmodule - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity topmodule is
    Port ( sixhundred : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           seg_final : out  STD_LOGIC_VECTOR (7 downto 0);
			  om : inout  STD_LOGIC_VECTOR (3 downto 0);
			  switch : inout  STD_LOGIC_VECTOR (1 downto 0);
			  seg : inout  STD_LOGIC_VECTOR (7 downto 0);
			  a : inout  STD_LOGIC_VECTOR (3 downto 0);
			  b : inout  STD_LOGIC_VECTOR (3 downto 0);
			  c : inout  STD_LOGIC_VECTOR (3 downto 0);
			  d : inout  STD_LOGIC_VECTOR (3 downto 0);
			  hundred : inout  STD_LOGIC;
			  second : inout  STD_LOGIC;
           strobe : out  STD_LOGIC_VECTOR (3 downto 0));
end topmodule;

architecture Behavioral of topmodule is

COMPONENT bcd27segmdecod
	PORT(
		om : IN std_logic_vector(3 downto 0);          
		seg : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

COMPONENT mux21
	PORT(
		a : IN std_logic_vector(3 downto 0);
		b : IN std_logic_vector(3 downto 0);
		c : IN std_logic_vector(3 downto 0);
		d : IN std_logic_vector(3 downto 0);
		switch : IN std_logic_vector(1 downto 0);    
		om : INOUT std_logic_vector(3 downto 0);      
		strobe : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

COMPONENT numbergen
	PORT(
		reset : IN std_logic;
		hundred : IN std_logic;
		second : IN std_logic;       
		switch : INOUT std_logic_vector(1 downto 0);
		a : INOUT std_logic_vector(3 downto 0);
		b : INOUT std_logic_vector(3 downto 0);
		c : INOUT std_logic_vector(3 downto 0);
		d : INOUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	COMPONENT divide
	PORT(
		sixhundred : IN std_logic;       
		hundred : INOUT std_logic;
		second : INOUT std_logic
		);
	END COMPONENT;

begin

Inst_bcd27segmdecod: bcd27segmdecod PORT MAP(
		om => om,
		seg => seg_final
	);
Inst_mux21: mux21 PORT MAP(
		a => a,
		b => b,
		c => c,
		d => d,
		strobe => strobe,
		switch => switch,
		om => om
	);
Inst_numbergen: numbergen PORT MAP(
		reset => reset,
		hundred => hundred,
		second => second,
		switch => switch,
		a => a,
		b => b,
		c => c,
		d => d
	);

Inst_divide: divide PORT MAP(
		sixhundred => sixhundred,
		hundred => hundred,
		second => second
	);
	
end Behavioral;

