--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:57:24 09/24/2013
-- Design Name:   
-- Module Name:   C:/alma/temp/mux32test.vhd
-- Project Name:  twodisplayassig
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: mux21
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY mux32test IS
END mux32test;
 
ARCHITECTURE behavior OF mux32test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mux21
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         c : IN  std_logic_vector(3 downto 0);
         d : IN  std_logic_vector(3 downto 0);
         strobe : OUT  std_logic_vector(3 downto 0);
         switch : IN  std_logic_vector(1 downto 0);
         om : INOUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal c : std_logic_vector(3 downto 0) := (others => '0');
   signal d : std_logic_vector(3 downto 0) := (others => '0');
   signal switch : std_logic_vector(1 downto 0) := (others => '0');

	--BiDirs
   signal om : std_logic_vector(3 downto 0);

 	--Outputs
   signal strobe : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mux21 PORT MAP (
          a => a,
          b => b,
          c => c,
          d => d,
          strobe => strobe,
          switch => switch,
          om => om
        );

 
 

   -- Stimulus process
   stim_proc: process
   begin		
	a<= "0000";
	b<= "0001";
	c<= "0010";
	d<= "0011";
	
	switch <="00";
	wait for 100 ns;	
	switch <="01";
	wait for 100 ns;	
	switch <="10";
	wait for 100 ns;	
	switch <="11";
	wait for 100 ns;	
	switch <="00";
	wait for 100 ns;	
	switch <="01";
	wait for 100 ns;	
	switch <="10";
	wait for 100 ns;	
	switch <="11";
	wait for 100 ns;	
	switch <="00";
	
      -- hold reset state for 100 ns.
      wait for 100 ns;	

    

      -- insert stimulus here 

      wait;
   end process;

END;
