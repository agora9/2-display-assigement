----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:46:23 09/23/2013 
-- Design Name: 
-- Module Name:    decoder12 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder12 is
    Port ( switch : in  STD_LOGIC;
           address : out  STD_LOGIC_VECTOR (1 downto 0));
end decoder12;

architecture Behavioral of decoder12 is

begin


process(switch)
begin

if (switch = '1') then 
address <= "01";
end if;

if (switch = '0') then 
address <= "10";
end if;

End process;

end Behavioral;

