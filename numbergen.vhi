
-- VHDL Instantiation Created from source file numbergen.vhd -- 15:20:12 09/24/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT numbergen
	PORT(
		reset : IN std_logic;
		hundred : IN std_logic;
		second : IN std_logic;       
		switch : INOUT std_logic_vector(1 downto 0);
		a : INOUT std_logic_vector(3 downto 0);
		b : INOUT std_logic_vector(3 downto 0);
		c : INOUT std_logic_vector(3 downto 0);
		d : INOUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_numbergen: numbergen PORT MAP(
		reset => ,
		hundred => ,
		second => ,
		switch => ,
		a => ,
		b => ,
		c => ,
		d => 
	);


