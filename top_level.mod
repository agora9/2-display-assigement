MODEL
MODEL_VERSION "v1998.8";
DESIGN "top_level";

/* port names and type */
INPUT S:PIN4 = disp_test<0>;
INPUT S:PIN5 = disp_test<1>;
INPUT S:PIN6 = disp_test<2>;
INPUT S:PIN7 = disp_test<3>;
OUTPUT S:PIN2 = seg<0>;
OUTPUT S:PIN140 = seg<1>;
OUTPUT S:PIN142 = seg<2>;
OUTPUT S:PIN3 = seg<3>;
OUTPUT S:PIN143 = seg<4>;
OUTPUT S:PIN138 = seg<5>;
OUTPUT S:PIN139 = seg<6>;
OUTPUT S:PIN137 = seg<7>;

/* timing arc definitions */
disp_test<1>_seg<0>_delay: DELAY disp_test<1> seg<0>;
disp_test<2>_seg<0>_delay: DELAY disp_test<2> seg<0>;
disp_test<3>_seg<0>_delay: DELAY disp_test<3> seg<0>;
disp_test<0>_seg<1>_delay: DELAY disp_test<0> seg<1>;
disp_test<1>_seg<1>_delay: DELAY disp_test<1> seg<1>;
disp_test<2>_seg<1>_delay: DELAY disp_test<2> seg<1>;
disp_test<3>_seg<1>_delay: DELAY disp_test<3> seg<1>;
disp_test<0>_seg<2>_delay: DELAY disp_test<0> seg<2>;
disp_test<1>_seg<2>_delay: DELAY disp_test<1> seg<2>;
disp_test<2>_seg<2>_delay: DELAY disp_test<2> seg<2>;
disp_test<3>_seg<2>_delay: DELAY disp_test<3> seg<2>;
disp_test<0>_seg<3>_delay: DELAY disp_test<0> seg<3>;
disp_test<1>_seg<3>_delay: DELAY disp_test<1> seg<3>;
disp_test<2>_seg<3>_delay: DELAY disp_test<2> seg<3>;
disp_test<3>_seg<3>_delay: DELAY disp_test<3> seg<3>;
disp_test<0>_seg<4>_delay: DELAY disp_test<0> seg<4>;
disp_test<1>_seg<4>_delay: DELAY disp_test<1> seg<4>;
disp_test<2>_seg<4>_delay: DELAY disp_test<2> seg<4>;
disp_test<3>_seg<4>_delay: DELAY disp_test<3> seg<4>;
disp_test<0>_seg<5>_delay: DELAY disp_test<0> seg<5>;
disp_test<1>_seg<5>_delay: DELAY disp_test<1> seg<5>;
disp_test<2>_seg<5>_delay: DELAY disp_test<2> seg<5>;
disp_test<3>_seg<5>_delay: DELAY disp_test<3> seg<5>;
disp_test<0>_seg<6>_delay: DELAY disp_test<0> seg<6>;
disp_test<1>_seg<6>_delay: DELAY disp_test<1> seg<6>;
disp_test<2>_seg<6>_delay: DELAY disp_test<2> seg<6>;
disp_test<3>_seg<6>_delay: DELAY disp_test<3> seg<6>;
disp_test<0>_seg<7>_delay: DELAY disp_test<0> seg<7>;
disp_test<1>_seg<7>_delay: DELAY disp_test<1> seg<7>;
disp_test<2>_seg<7>_delay: DELAY disp_test<2> seg<7>;
disp_test<3>_seg<7>_delay: DELAY disp_test<3> seg<7>;

/* timing check arc definitions */

ENDMODEL
