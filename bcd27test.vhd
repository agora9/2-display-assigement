--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:55:55 09/24/2013
-- Design Name:   
-- Module Name:   C:/alma/temp/bcd27test.vhd
-- Project Name:  twodisplayassig
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bcd27segmdecod
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY bcd27test IS
END bcd27test;
 
ARCHITECTURE behavior OF bcd27test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bcd27segmdecod
    PORT(
         om : IN  std_logic_vector(3 downto 0);
         seg : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal om : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal seg : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
  
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bcd27segmdecod PORT MAP (
          om => om,
          seg => seg
        );

  
 

   -- Stimulus process
   stim_proc: process
   begin

	
      -- hold reset state for 100 ns.
      wait for 100 ns;	

    

      -- insert stimulus here 

      wait;
   end process;

END;
