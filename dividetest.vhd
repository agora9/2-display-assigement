--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:43:37 09/24/2013
-- Design Name:   
-- Module Name:   C:/alma/temp/dividetest.vhd
-- Project Name:  twodisplayassig
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: divide
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY dividetest IS
END dividetest;
 
ARCHITECTURE behavior OF dividetest IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT divide
    PORT(
         sixhundred : IN  std_logic;
         hundred : INOUT  std_logic;
         second : INOUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal sixhundred : std_logic := '0';

	--BiDirs
   signal hundred : std_logic;
   signal second : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
  
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: divide PORT MAP (
          sixhundred => sixhundred,
          hundred => hundred,
          second => second
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
	

	for I in 0 to 3900000 loop
	sixhundred <= '1';
	wait for 0.0016 ms;	
	sixhundred <= '0';
	wait for 0.0016 ms;
	end loop;
      -- hold reset state for 100 ns.
	 

     

      -- insert stimulus here 

      wait;
   end process;

END;
