----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:51:54 09/24/2013 
-- Design Name: 
-- Module Name:    numbergen - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity numbergen is
    Port ( reset : in  STD_LOGIC;
           hundred : in  STD_LOGIC;
           second : in  STD_LOGIC;
           switch : inout  STD_LOGIC_VECTOR (1 downto 0);
           a : inout  STD_LOGIC_VECTOR (3 downto 0);
           b : inout  STD_LOGIC_VECTOR (3 downto 0);
           c : inout  STD_LOGIC_VECTOR (3 downto 0);
           d : inout  STD_LOGIC_VECTOR (3 downto 0));
end numbergen;

architecture Behavioral of numbergen is

signal secondsig, tensecondsig, minutesig, tenminutesig : STD_LOGIC_VECTOR (3 downto 0) := "0000"; 
signal secondrest, tensecondrest, minuterest, tenminuterest : STD_LOGIC := '0'; 
signal hudredsig : STD_LOGIC_VECTOR (1 downto 0) := "00";


begin

Process (reset, second)
begin

if rising_edge(reset) then
secondsig <= "0000";
end if;	

if rising_edge(second) then
	if (secondsig = "1001") then
	secondrest <= not secondrest;
	secondsig <= "0000";
	End if;
	if (secondsig /= "1001") then
	secondsig <= secondsig +1;
	End if;
End if;
end process;

process (reset, secondrest)
begin

if rising_edge(reset) then
tensecondsig <= "0000";
end if;
 
if (secondrest' event) then
	
	if (tensecondsig = "0110") then
	tensecondrest <= not tensecondrest;
	tensecondsig <= "0000";
	end if;
	
	if (tensecondsig /= "0110") then
	tensecondsig <= tensecondsig +1;
	end if;
end if;

end process;

process (reset, tensecondrest)
begin

if rising_edge(reset) then
minutesig <= "0000";
end if;

if (tensecondrest' event) then

	if (minutesig = "1001") then
	minuterest <= not minuterest;
	minutesig <= "0000";
	end if;
	if (minutesig /= "1001") then
	minutesig <= minutesig +1;
	end if;
end if;

end process;


process (reset, minuterest)
begin

if rising_edge(reset) then
tenminutesig <= "0000";
end if;

if (minuterest' event) then
	if (tenminutesig = "1001") then

	tenminutesig <= "0000";
	end if;
	if (tenminutesig /= "1001") then
	tenminutesig <= tenminutesig +1;
	end if;
end if;

end process;

process(hundred)
begin

if	rising_edge(hundred) then 

if(hudredsig = "00") then
hudredsig <= "01";
end if;

if(hudredsig = "01") then
hudredsig <= "10";
end if;

if(hudredsig = "10") then 
hudredsig <= "11";
end if;

if(hudredsig = "11") then
hudredsig <= "00";
end if;

end if;
end process;

switch <= hudredsig;
a <= secondsig;
b <= tensecondsig;
c <= minutesig;
d <= tenminutesig;

end Behavioral;

