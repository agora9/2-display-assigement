--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:08:06 09/24/2013
-- Design Name:   
-- Module Name:   C:/alma/temp/numbergentest.vhd
-- Project Name:  twodisplayassig
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: numbergen
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY numbergentest IS
END numbergentest;
 
ARCHITECTURE behavior OF numbergentest IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT numbergen
    PORT(
         reset : IN  std_logic;
         hundred : IN  std_logic;
         second : IN  std_logic;
         switch : INOUT  std_logic_vector(1 downto 0);
         a : INOUT  std_logic_vector(3 downto 0);
         b : INOUT  std_logic_vector(3 downto 0);
         c : INOUT  std_logic_vector(3 downto 0);
         d : INOUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal reset : std_logic := '0';
   signal hundred : std_logic := '0';
   signal second : std_logic := '0';

	--BiDirs
   signal switch : std_logic_vector(1 downto 0);
   signal a : std_logic_vector(3 downto 0);
   signal b : std_logic_vector(3 downto 0);
   signal c : std_logic_vector(3 downto 0);
   signal d : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
  
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: numbergen PORT MAP (
          reset => reset,
          hundred => hundred,
          second => second,
          switch => switch,
          a => a,
          b => b,
          c => c,
          d => d
        );

   
 

   -- Stimulus process
   stim_proc: process
   begin
	
	for I in 0 to 6300 loop
	second <= '1';
		wait for 1 ps;
		second <= '0';
		wait for 1 ps;
	end loop;
		 
		 
				
      -- hold reset state for 100 ns.
      wait for 100 ns;	

     

      -- insert stimulus here 

      wait;
   end process;

END;
