
-- VHDL Instantiation Created from source file divide.vhd -- 15:21:05 09/24/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT divide
	PORT(
		sixhundred : IN std_logic;       
		hundred : INOUT std_logic;
		second : INOUT std_logic
		);
	END COMPONENT;

	Inst_divide: divide PORT MAP(
		sixhundred => ,
		hundred => ,
		second => 
	);


