----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:39:18 09/24/2013 
-- Design Name: 
-- Module Name:    divide - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity divide is
    Port ( sixhundred : in  STD_LOGIC:='0';
           hundred : inout  STD_LOGIC:='0';
           second : inout  STD_LOGIC:='0');
end divide;

architecture Behavioral of divide is

signal cnt : STD_LOGIC_VECTOR (11 downto 0) := "000000000000";
signal clock100 : STD_LOGIC:= '0';

signal cnt2 : STD_LOGIC_VECTOR (6 downto 0) := "0000000";
signal clock1 : STD_LOGIC:= '0';

begin

process (sixhundred)
begin

if rising_edge(sixhundred) then
if(cnt = "000010111000") then
--if(cnt = "101110111000") then 1es le lett t�r�lve az elej�r�l
	cnt <= "000000000000";
	clock100 <= not clock100;
else
		cnt <= cnt + 1;
end if;
end if;
end process;

process (clock100)

begin

if rising_edge(clock100) then

if (cnt2 = "1100111") then
--if (cnt2 = "1100100") then
	cnt2 <= "0000000";
	clock1 <= not clock1;
else
	cnt2 <= cnt2+1;
end if;
end if;

hundred <= clock100;
second <= clock1;

end process;

end Behavioral;

