
-- VHDL Instantiation Created from source file mux21.vhd -- 14:46:39 09/24/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT mux21
	PORT(
		a : IN std_logic_vector(3 downto 0);
		b : IN std_logic_vector(3 downto 0);
		c : IN std_logic_vector(3 downto 0);
		d : IN std_logic_vector(3 downto 0);
		switch : IN std_logic_vector(1 downto 0);    
		om : INOUT std_logic_vector(3 downto 0);      
		strobe : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_mux21: mux21 PORT MAP(
		a => ,
		b => ,
		c => ,
		d => ,
		strobe => ,
		switch => ,
		om => 
	);


