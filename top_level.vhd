----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:58:53 09/23/2013 
-- Design Name: 
-- Module Name:    top_level - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_level is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
           switch : in  STD_LOGIC;
           address : out  STD_LOGIC_VECTOR (1 downto 0);
			  disp_temp : inout  STD_LOGIC_VECTOR (3 downto 0);
           seg : out  STD_LOGIC_VECTOR (7 downto 0));
end top_level;

architecture Behavioral of top_level is


	COMPONENT bcd27segmdecod
	PORT(
		om : IN std_logic_vector(3 downto 0);          
		seg : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
		COMPONENT decoder12
	PORT(
		switch : IN std_logic;          
		address : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;

	COMPONENT mux21
	PORT(
		a : IN std_logic_vector(3 downto 0);
		b : IN std_logic_vector(3 downto 0);
		switch : IN std_logic;       
		om : INOUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

begin

Inst_bcd27segmdecod: bcd27segmdecod PORT MAP(
		om => disp_temp ,
		seg => seg
	);

Inst_decoder12: decoder12 PORT MAP(
		switch => switch,
		address => address
	);
	
Inst_mux21: mux21 PORT MAP(
		a => a,
		b => b,
		switch => switch,
		om => disp_temp
	);


end Behavioral;

